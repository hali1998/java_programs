package AutomationPractice;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class JavascriptExecutorConcept {

	public static void main(String[] args) throws IOException, InterruptedException {


		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\kk631\\\\Downloads\\\\selenium\\\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		//driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.get("https://freecrm.co.in/");
		WebElement element=driver.findElement(By.xpath("//span[text()='Log In']"));
		//flash(element,driver);
		setBorderByJs(element,driver);
		//File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		//FileUtils.copyFile(src, new File("C:\\Users\\kk631\\Documents\\SchotSelenium.png"));
		//generateAlert(driver,"This is an issue");
		//Alert alert=driver.switchTo().alert();
		//alert.accept();
		//clickElementByJs(element,driver);
		//refreshBrowserByJs(driver);
		System.out.println(getTitleByJs(driver));
		//System.out.println(getPageTextByJs(driver));
		//scrollPageByJs(driver);
		scrollUptoElementByJs(driver.findElement(By.xpath("//h3[@class='divider-sm divider-sm-mod-1 text-center']")),driver);


	}

	public static void flash(WebElement element,WebDriver driver)
	{
		String bgColor=element.getCssValue("backgroundColor");
		for(int i=0;i<100;i++)
		{
			changeColor("(255,255,0)",element,driver);
			//changeColor(bgColor,element,driver);
		}
	}
	public static void changeColor(String color,WebElement element,WebDriver driver)
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.backgroundColor ='"+color+"'", element);
		try
		{
			Thread.sleep(4000);
		}
		catch(InterruptedException e)
		{

		}

	}
	public static void setBorderByJs(WebElement element,WebDriver driver) //to draw a border
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.border='3px solid red'", element);
	}
	public static void generateAlertByJs(WebDriver driver,String message) //to generate alert
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("alert('"+message+"')");
	}
	public static void clickElementByJs(WebElement element,WebDriver driver) //click element
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
	}
	public static void refreshBrowserByJs(WebDriver driver) //refresh Browser
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("history.go(0)");
	}
	public static String getTitleByJs(WebDriver driver) //to get title by JS
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		String title= js.executeScript("return document.title;").toString();
		return title;

	}
	public static String getPageTextByJs(WebDriver driver) //to get the inner text by JS
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		return js.executeScript("return document.documentElement.innerText").toString();
	}
	public static void scrollPageByJs(WebDriver driver)
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
	}
	public static void scrollUptoElementByJs(WebElement element,WebDriver driver) throws InterruptedException
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);",element);
		Thread.sleep(2000);
	}

}
