package AutomationPractice;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CalenderSelectConcept {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\kk631\\\\Downloads\\\\selenium\\\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.seleniumeasy.com/test/jquery-date-picker-demo.html");
		driver.findElement(By.id("from")).click();
		String date="31-Jun-2017";
			String dateArray[]=	date.split("-");
			String day=dateArray[0];
			String month=dateArray[1];
			String year=dateArray[2];
			Select select=new Select(driver.findElement(By.xpath("//select[@class='ui-datepicker-month']")));
			select.selectByVisibleText(month);
			final int weekDays=7;
			boolean flag=false;
			String dayVal=null;
			// /html[1]/body[1]/div[3]/table[1]/tbody[1]/tr[1]/td[2]/a[1]
			///html[1]/body[1]/div[3]/table[1]/tbody[1]/tr[1]/td[4]/a[1]
			String beforeXpath="/html[1]/body[1]/div[3]/table[1]/tbody[1]/tr[";
			String afterXpath="]/td[";
			for(int rowNum=1;rowNum<=6;rowNum++)
			{
				for(int colNum=1;colNum<=weekDays;colNum++)
				{
					try
					{
					 dayVal=driver.findElement(By.xpath(beforeXpath+rowNum+afterXpath+colNum+"]")).getText();
					}catch(NoSuchElementException e)
					{
						System.out.println("Please enter a correct date");
						flag=false;
						break;
						
					}
					System.out.println(dayVal);
					if(dayVal.equals(day))
					{
						driver.findElement(By.xpath(beforeXpath+rowNum+afterXpath+colNum+"]/a[1]")).click();
						flag=true;
						break;
					}
					
				}
				if(flag)
					break;
			}
			
			
			
			
}
}
