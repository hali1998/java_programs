package AutomationPractice;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class ObjectRepository {
	static WebDriver driver;

	public static void main(String[] args) throws IOException {

		Properties properties =new Properties();
		FileInputStream fileInputStream =new FileInputStream("C:\\Users\\kk631\\eclipse-workspace\\SeleniumAutomationByNaveen\\src\\AutomationPractice\\Config.properties");
		properties.load(fileInputStream);
		System.out.println(properties.getProperty("Age"));
		String browserName=properties.getProperty("browser");
		String url=properties.getProperty("Url");
		if(browserName.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\kk631\\Downloads\\selenium\\chromedriver.exe");
			driver=new ChromeDriver();
		}
		else
		{
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\kk631\\Downloads\\selenium\\geckodriver.exe");
			driver =new FirefoxDriver();
		}
		driver.get(url);
		driver.findElement(By.xpath(properties.getProperty("first_xpath"))).sendKeys(properties.getProperty("phone"));
		driver.findElement(By.xpath(properties.getProperty("pass_xpath"))).sendKeys(properties.getProperty("password"));
		driver.findElement(By.xpath(properties.getProperty("Submit"))).click();
	
		

	}
	
}
