package AutomationPractice;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElementsConcept {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\kk631\\\\Downloads\\\\selenium\\\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(7, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.get("https://wapking.online/");
		List<WebElement> linkedList =driver.findElements(By.tagName("a"));
		System.out.println(linkedList.size());
		for(int i=0;i<linkedList.size();i++)
		{
			System.out.println(linkedList.get(i).getText());
		}
		driver.quit();
		
		
	}

}
