package AutomationPractice;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HighlighingELement {
	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\kk631\\\\Downloads\\\\selenium\\\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://www.seleniumeasy.com/test/");
		WebElement ele=driver.findElement(By.xpath("//a[text()='Input Forms']"));
		clickElement(driver,ele);
		ele.click();
		
	}
	
	public static void clickElement(WebDriver driver,WebElement element)
	{
		highLighterMethod(driver,element);
	}
	public static void highLighterMethod(WebDriver driver,WebElement element)
	{
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].setAttribute('style','background:yellow;border:2px solid red;');" ,element);
		
		
	}

}
