package AutomationPractice;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleSearchTest {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\kk631\\\\Downloads\\\\selenium\\\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("http://www.google.com");
		driver.findElement(By.xpath("//input[@title='Search']")).sendKeys("testing");
		Thread.sleep(4000);
		List<WebElement>data=driver.findElements(By.xpath("//ul[@role='listbox']//li"));
		System.out.println("Total no of suggestions :"+data.size());
		for(int i=0;i<data.size();i++)
		{
			System.out.println(data.get(i).getText());
			if(data.get(i).getText().contains("testing tutorial"))
			{
				data.get(i).click();
				break;
			}
		}
		
	}

}
