package AutomationPractice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExplicitlyWaitConcept {

	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\kk631\\\\Downloads\\\\selenium\\\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://www.seleniumeasy.com/test/");
		explicitlyWait(driver,driver.findElement(By.xpath("(//ul[@class=\"nav navbar-nav\"]//a[@class='dropdown-toggle'])[1]")),10);
		Thread.sleep(3000);
		explicitlyWait(driver,driver.findElement(By.xpath("//ul[@class=\"dropdown-menu\"]//a[text()=\"Simple Form Demo\"]")),10);
		driver.findElement(By.xpath("//input[@id=\"user-message\"]")).sendKeys("Hi this is Hasan here");
		driver.findElement(By.xpath("//button[text()=\"Show Message\"]")).click();
		driver.quit();

	}
	public static void explicitlyWait(WebDriver driver,WebElement element,int timeout)
	{
		new WebDriverWait(driver,timeout).ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}
	public static String getMessage() throws IOException
	{
		BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter your message:");
		return reader.readLine();
	}
}
