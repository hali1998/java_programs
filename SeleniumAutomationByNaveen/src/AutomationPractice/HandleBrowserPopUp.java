package AutomationPractice;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandleBrowserPopUp {

	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\kk631\\\\Downloads\\\\selenium\\\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("http://popuptest.com/");
		driver.findElement(By.xpath("//a[text()='Come & Go Test']")).click();
		Set<String>handle=driver.getWindowHandles();
		Iterator<String>iterator=handle.iterator();
		String parentId=iterator.next();
		System.out.println(parentId);
		String childId=iterator.next();
		System.out.println(childId);
		driver.switchTo().window(childId);
		System.out.println(driver.getTitle());
		driver.switchTo().window(parentId);
		System.out.println(driver.getTitle());
		driver.close();
		

	}

}
