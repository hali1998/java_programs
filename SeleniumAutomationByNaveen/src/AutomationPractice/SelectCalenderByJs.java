package AutomationPractice;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SelectCalenderByJs {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\kk631\\\\Downloads\\\\selenium\\\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		//driver.manage().timeouts().pageLoadTimeout(4, TimeUnit.SECONDS);
		driver.get("https://www.spicejet.com");
		//Thread.sleep(5000);
		WebElement element=driver.findElement(By.xpath("//input[@id='//input[@id='ctl00_mainContent_txt_Fromdate']']"));
		String dateVal="29-12-2019";
		selectDateByJs(driver,element,dateVal);
		
		
	}
	public static void selectDateByJs(WebDriver driver,WebElement element,String date) throws InterruptedException
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('value','"+date+"');", element);
		
	}
}
