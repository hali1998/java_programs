package AutomationPractice;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DynamicWebTableHandle {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\kk631\\\\Downloads\\\\selenium\\\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://freecrm.co.in/");
		driver.findElement(By.xpath("//span[text()='Log In']")).click();
		driver.findElement(By.name("email")).sendKeys("kk631352@gmail.com");
		driver.findElement(By.name("password")).sendKeys("#Import#1");
		driver.findElement(By.xpath("//div[text()=\"Login\"]")).click();
		driver.switchTo().frame("downloadFrame");
		WebElement element=driver.findElement(By.xpath("//span[contains(text(),'Contacts')]"));
		clickElementByJs(driver,element);
		
		////div[@class='ui fluid container main-content']/table/tbody/tr[1]/td[2]
		String before_xpath="//div[@class='ui fluid container main-content']/table/tbody/tr[";
		String after_xpath="]/td[2]";
		
		for(int i=1;i<=2;i++)
		{
			String name=driver.findElement(By.xpath(before_xpath+i+after_xpath)).getText();
			System.out.println(name);
		}
		//Method-2
		//driver.findElement(By.xpath("//a[contains(text(),'rowName')]/parent::td//preceding-sibling::td//input"))

	}
	public static void clickElementByJs(WebDriver driver,WebElement element)
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();",element);
	}

}
